var request = require('request')
  , when = require('when')
  , nodefn = require('when/node/function')

/**
 * Makes an HTTP request with `options`, returning a promise to be fulfilled
 * with the response.
 */
function pRequest(options) {
  var deferred = when.defer()
    , callback = nodefn.createCallback(deferred)

  request(options, callback)

  return deferred.promise
}

module.exports = {
  pRequest: pRequest
}
