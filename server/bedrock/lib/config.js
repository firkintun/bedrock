var rc = require('rc')

function getHostname() {
  return process.env.HOSTNAME ||
    process.env.OPENSHIFT_NODE_IP ||
    null
}

function getPort() {
  return process.env.PORT ||
    process.env.OPENSHIFT_NODE_PORT ||
    8080
}

function getMongoUrl(name) {
  return process.env.MONGO_URL ||
    process.env.OPENSHIFT_MONGODB_DB_URL ||
    process.env.MONGOLAB_URI ||
    process.env.MONGOHQ_URL ||
    'mongodb://127.0.0.1:27017/' + name
}

function getCouchUrl() {
  return process.env.COUCH_URL ||
    process.env.OPENSHIFT_COUCHDB_DB_URL ||
    process.env.CLOUDANT_URL ||
    'http://127.0.0.1:5984'
}

function getCombinedConfig(name) {
  return rc(name, {
    name: name,
    hostname: getHostname(),
    port: getPort(),
    mongoUrl: getMongoUrl(name),
    couchUrl: getCouchUrl()
  })
}

module.exports = getCombinedConfig
