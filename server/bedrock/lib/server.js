'use strict'
var http = require('http')
  , util = require('util')
  , mach = require('mach')
  , mi = require('mi')
  , when = require('when')
  , logger = require('./logger')
  , INTERNAL_ERROR_TYPES = ['TypeError', 'ReferenceError', 'RangeError']

/**
 * Creates a new instance of Server with the provided `options`.
 *
 * @param {Object} options
 */
function Server(options) {
  if (!(this instanceof Server)) {
    return new Server(options);
  }

  options = options || {}

  this.name = options.name || 'unknown'
  this.hostname = options.hostname || null
  this.port = options.port || 5000
  this.silent = options.silent || false

  this.logger = logger(this.name + ':Server')
  this.stack = mach.stack()
  this._server = null

  this._initRoutes()
}
Server.inherit = mi.inherit
Server.extend = mi.extend

/**
 * Returns a new Server instance. If called on a Server subclass, that subclass
 * will be used instead.
 */
Server.createServer = createServer
function createServer(options) {
  var Constructor = this;
  return new Constructor(options);
}

/**
 * Returns a promise to be fulfilled with content appropriate for the given
 * status code. If provided as an Object, `content` will be used as the JSON
 * body. If provided as a String or Buffer, `content` will be used as-is.
 * Otherwise, an empty object will be used. Likewise, `headers` will be used if
 * provided.
 */
Server.response = response
function response(status, content, headers) {
  if (typeof content === 'object') {
    content = JSON.stringify(content || {})
  } else if (typeof content.length !== 'number') {
    content = '{}'
  }

  headers = util._extend({
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(content)
  }, headers)

  return when.resolve({
    status: status,
    content: content,
    headers: headers
  })
}

/**
 * Returns a promise to be fulfilled with content appropriate for the given
 * successful status code. If provided, `content` will be used as the JSON
 * body. Otherwise, an empty body will be used.
 */
Server.success = success
function success(status, content) {
  if (arguments.length === 1) {
    content = status
    status = null
  }

  return response(status || 200, content)
}

/**
 * Returns a promise to be fulfilled with content appropriate for the given
 * error.
 */
Server.failure = failure
function failure(error) {
  if (INTERNAL_ERROR_TYPES.indexOf(error.name) !== -1) {
    // Bubble and let Mach handle it.
    return when.reject(error)
  }

  return Server.badRequest(400, error.message || error)
}

/**
 * Returns a promise to be fulfilled with content appropriate for the given
 * error status code. If provided, `details` will be added to the JSON body
 * as an additional field.
 */
Server.badRequest = badRequest
function badRequest(status, details) {
  var content = JSON.stringify({
    error: http.STATUS_CODES[status] || 'Unknown Error',
    details: details || ''
  })

  return when.resolve({
    status: status || 400,
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(content)
    },
    content: content
  })
}

/**
 * Starts the Server, returning a promise to be fulfilled once the Server
 * is listening. If an error occurs in the duration, the promise will be
 * rejected.
 */
Server.prototype.start = start
function start() {
  var self = this
    , deferred = when.defer()

  self._server = mach.serve(self.stack, {
    host: self.hostname,
    port: self.port,
    quiet: true
  })
    .once('listening', function () {
      self.hostname = self._server.address().address
      self.port = self._server.address().port

      deferred.resolve()
    })
    .once('error', deferred.reject)

  return deferred.promise
    .then(function () {
      self.logger.info('Listening at %s:%s...', self.hostname, self.port)
    })
}

/**
 * Stops the Server, returning a promise to be fulfilled once the Server
 * is no longer listening. If an error occurs in the duration, the promise
 * will be rejected.
 */
Server.prototype.stop = stop
function stop() {
  var self = this
    , deferred = when.defer()

  self._server
    .once('close', deferred.resolve)
    .once('error', deferred.reject)
    .close()

  return deferred.promise
}

/**
 * Binds routes to both internal methods and queries on User.
 */
Server.prototype._initRoutes = _initRoutes
function _initRoutes() {
  var self = this

  if (!self.silent) {
    self.stack.use(this.logger.mach)
  }

  self.stack.use(mach.params)

  return self
}

/*!
 * Export `Server`.
 */
module.exports = Server
