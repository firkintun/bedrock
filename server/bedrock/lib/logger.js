var fs = require('fs')
  , path = require('path')
  , stream = require('stream')
  , sawmill = require('sawmill')
  , container = { _loggers: {} }

// A trivial container for sharing similarly-named Mills.
container.get = function(name) {
  var mill = this._loggers[name]

  if (!mill) {
    mill = this._loggers[name] = sawmill.createMill(name)

    mill.add('human', sawmill.human, 'debug')
    mill.add('file', fs.createWriteStream(
      // HACK(schoon) - Find a better path for this.
      path.resolve(__dirname, '..', '..', '..', 'log'),
      {
        flags: 'a'
      }
    ))
  }

  return mill
}

// Creates a new, named Mill with the usual transports.
function newLogger(name) {
  var logger = container.get(name)

  logger.mach = machLogger

  return logger

  // Available as logger.mach, this Mach middleware logs all requests made and
  // responses sent.
  function machLogger(app, level) {
    level = level || 'info'

    return function log(request) {
      var start = process.hrtime()

      return request.call(app).then(function (response) {
        var diff = process.hrtime(start)
          , ms = (diff[0] * 1e6 + diff[1] / 1e3)

        response.content = teeFirstChunk(response.content, function (chunk) {
          var content = String(chunk)
            , obj

          try {
            content = JSON.parse(content)
          } catch (e) {
          }

          obj = {
            protocol: 'HTTP/' + request.protocolVersion,
            route: request.method + ' ' + request.fullPath,
            params: request.params,
            status: response.status,
            duration: ms,
            content: content
          }

          logger.info(obj, obj.status + ' ' + obj.route)
        })

        return response
      })
    }
  }

  // Calls `handler` with the first chunk received from `input`, returning a
  // PassThrough stream that sends along all chunkd from `input`.
  function teeFirstChunk(input, handler) {
    var output = new stream.PassThrough()

    output._transform = function (chunk, encoding, callback) {
      output._transform = stream.PassThrough.prototype._transform

      handler(chunk)

      output.push(chunk)
      callback()
    }

    input.pipe(output)
    return output
  }
}

module.exports = newLogger
