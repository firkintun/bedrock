if keys | contains(["route"]) then
  "\(.mill) | \(.level): \(.status) \(.route)", .params, .content
else
  "\(.mill) | \(.level): \(.message)"
end
